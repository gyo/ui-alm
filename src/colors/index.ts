type Color = {
  hue: number;
  saturation: number;
  lightness: number;
};

const colorBlindnessTypes = [
  "protanopia",
  "protanomaly",
  "deuteranopia",
  "deuteranomaly",
  "tritanopia",
  "tritanomaly",
  "achromatopsia",
  "achromatomaly"
];

const normalizeHue = (hue: number) => {
  let h = hue;
  while (h < 0) {
    h = h + 360;
  }
  return Math.round(h % 360);
};

const normalizeSaturation = (saturation: number) => {
  if (saturation < 0) {
    return 0;
  }
  if (100 < saturation) {
    return 100;
  }
  return Math.round(saturation);
};

const normalizeLightness = (lightness: number) => {
  if (lightness < 0) {
    return 0;
  }
  if (100 < lightness) {
    return 100;
  }
  return Math.round(lightness);
};

const getHslStyleValue = (color: Color) => {
  return `hsl(${color.hue}, ${color.saturation}%, ${color.lightness}%)`;
};

const getHexStyleValue = (color: Color) => {
  return HSLToHex(color.hue, color.saturation, color.lightness);
};

const getNumberValue = (element: HTMLInputElement) => {
  const value = element.value;
  if (value === "") {
    return "";
  }
  const number = parseInt(value);
  if (isNaN(number)) {
    return "";
  }
  return number;
};

const applyColorBlindnessFilter = (element: HTMLElement, type: string) => {
  element.classList.remove(...colorBlindnessTypes);
  if (type !== "") {
    element.classList.add(type);
  }
};

const removeChildren = (element: HTMLElement) => {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
};

const refreshList = (colors: Color[], element: HTMLOListElement) => {
  removeChildren(element);

  const fragment = document.createDocumentFragment();

  colors.forEach((color, index) => {
    const hslStyleValue = getHslStyleValue(color);
    const hexStyleValue = getHexStyleValue(color);

    const li = document.createElement("li");

    const tile = document.createElement("div");
    tile.classList.add("tile");
    tile.textContent = `${index}`;
    tile.style.backgroundColor = hslStyleValue;
    li.appendChild(tile);

    const hsl = document.createElement("div");
    hsl.classList.add("hsl");
    hsl.textContent = hslStyleValue;
    li.appendChild(hsl);

    const hex = document.createElement("div");
    hex.classList.add("hex");
    hex.textContent = hexStyleValue;
    li.appendChild(hex);

    fragment.appendChild(li);
  });

  element.appendChild(fragment);
};

// https://css-tricks.com/converting-color-spaces-in-javascript/
const HSLToHex = (h: number, s: number, l: number) => {
  s /= 100;
  l /= 100;

  let c = (1 - Math.abs(2 * l - 1)) * s,
    x = c * (1 - Math.abs(((h / 60) % 2) - 1)),
    m = l - c / 2,
    r = 0,
    g = 0,
    b = 0;

  if (0 <= h && h < 60) {
    r = c;
    g = x;
    b = 0;
  } else if (60 <= h && h < 120) {
    r = x;
    g = c;
    b = 0;
  } else if (120 <= h && h < 180) {
    r = 0;
    g = c;
    b = x;
  } else if (180 <= h && h < 240) {
    r = 0;
    g = x;
    b = c;
  } else if (240 <= h && h < 300) {
    r = x;
    g = 0;
    b = c;
  } else if (300 <= h && h < 360) {
    r = c;
    g = 0;
    b = x;
  }
  // Having obtained RGB, convert channels to hex
  let sr = Math.round((r + m) * 255).toString(16);
  let sg = Math.round((g + m) * 255).toString(16);
  let sb = Math.round((b + m) * 255).toString(16);

  // Prepend 0s, if necessary
  if (sr.length == 1) sr = "0" + sr;
  if (sg.length == 1) sg = "0" + sg;
  if (sb.length == 1) sb = "0" + sb;

  return "#" + sr + sg + sb;
};

const copyColorFunction = () => {
  let timerId: number | undefined;

  const notification = document.querySelector(".notification");
  const listElements = document.querySelectorAll<HTMLOListElement>("ol");
  if (notification == null) {
    return;
  }
  listElements.forEach(listElement => {
    listElement.addEventListener("click", e => {
      const target = e.target as HTMLElement;
      if (target == null) {
        return;
      }
      const classList = target.classList;
      if (
        target.tagName === "DIV" &&
        (classList.contains("hsl") || classList.contains("hex")) &&
        target.textContent != null
      ) {
        const trimmedTextContent = target.textContent.trim();
        navigator.clipboard.writeText(trimmedTextContent).then(
          () => {
            if (timerId != null) {
              clearTimeout(timerId);
            }
            notification.classList.add("notification_open");
            notification.textContent = `${trimmedTextContent} を Clipboard にコピーしました。`;
            timerId = setTimeout(() => {
              notification.classList.remove("notification_open");
              notification.textContent = "";
            }, 1250);
          },
          () => {
            if (timerId != null) {
              clearTimeout(timerId);
            }
            notification.classList.add("notification_error");
            notification.classList.add("notification_open");
            notification.textContent = "Clipboard へのコピーに失敗しました。";
            timerId = setTimeout(() => {
              notification.classList.remove("notification_open");
              notification.textContent = "";
            }, 1250);
          }
        );
      }
    });
  });
};

const sequentialLightnessFunction = () => {
  const sequentialLightnessElement = document.querySelector(
    ".sequential-lightness"
  );
  if (sequentialLightnessElement == null) {
    return;
  }
  const hueElement = sequentialLightnessElement.querySelector<HTMLInputElement>(
    '[name="hue"]'
  );
  const saturationElement = sequentialLightnessElement.querySelector<
    HTMLInputElement
  >('[name="saturation"]');
  const lightnessElement = sequentialLightnessElement.querySelector<
    HTMLInputElement
  >('[name="lightness"]');
  const lightnessStepElement = sequentialLightnessElement.querySelector<
    HTMLInputElement
  >('[name="lightness-step"]');
  const blindnessElement = sequentialLightnessElement.querySelector<
    HTMLSelectElement
  >('[name="blindness"]');
  const resultElement = sequentialLightnessElement.querySelector<
    HTMLOListElement
  >(".result");
  if (
    hueElement == null ||
    saturationElement == null ||
    lightnessElement == null ||
    lightnessStepElement == null ||
    blindnessElement == null ||
    resultElement == null
  ) {
    return;
  }

  const reflectInputValueToList = () => {
    const hue = getNumberValue(hueElement);
    const saturation = getNumberValue(saturationElement);
    const lightness = getNumberValue(lightnessElement);
    const lightnessStep = getNumberValue(lightnessStepElement);
    if (
      hue === "" ||
      saturation === "" ||
      lightness === "" ||
      lightnessStep === ""
    ) {
      return;
    }
    const baseColor: Color = { hue, saturation, lightness };

    const colors = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(n => {
      return {
        ...baseColor,
        lightness: normalizeLightness(baseColor.lightness + n * lightnessStep)
      };
    });

    refreshList(colors, resultElement);
  };

  reflectInputValueToList();
  hueElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  saturationElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  lightnessElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  lightnessStepElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  blindnessElement.addEventListener("change", () => {
    applyColorBlindnessFilter(resultElement, blindnessElement.value);
  });
};

const sequentialHueLightnessFunction = () => {
  const sequentialHueLightnessElement = document.querySelector(
    ".sequential-hue-lightness"
  );
  if (sequentialHueLightnessElement == null) {
    return;
  }
  const hueElement = sequentialHueLightnessElement.querySelector<
    HTMLInputElement
  >('[name="hue"]');
  const saturationElement = sequentialHueLightnessElement.querySelector<
    HTMLInputElement
  >('[name="saturation"]');
  const lightnessElement = sequentialHueLightnessElement.querySelector<
    HTMLInputElement
  >('[name="lightness"]');
  const hueStepElement = sequentialHueLightnessElement.querySelector<
    HTMLInputElement
  >('[name="hue-step"]');
  const lightnessStepElement = sequentialHueLightnessElement.querySelector<
    HTMLInputElement
  >('[name="lightness-step"]');
  const blindnessElement = sequentialHueLightnessElement.querySelector<
    HTMLSelectElement
  >('[name="blindness"]');
  const resultElement = sequentialHueLightnessElement.querySelector<
    HTMLOListElement
  >(".result");
  if (
    hueElement == null ||
    saturationElement == null ||
    lightnessElement == null ||
    hueStepElement == null ||
    lightnessStepElement == null ||
    blindnessElement == null ||
    resultElement == null
  ) {
    return;
  }

  const reflectInputValueToList = () => {
    const hue = getNumberValue(hueElement);
    const saturation = getNumberValue(saturationElement);
    const lightness = getNumberValue(lightnessElement);
    const hueStep = getNumberValue(hueStepElement);
    const lightnessStep = getNumberValue(lightnessStepElement);
    if (
      hue === "" ||
      saturation === "" ||
      lightness === "" ||
      hueStep === "" ||
      lightnessStep === ""
    ) {
      return;
    }
    const baseColor: Color = { hue, saturation, lightness };

    const colors = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(n => {
      return {
        ...baseColor,
        hue: normalizeHue(baseColor.hue + n * hueStep),
        lightness: normalizeLightness(baseColor.lightness + n * lightnessStep)
      };
    });

    refreshList(colors, resultElement);
  };

  reflectInputValueToList();
  hueElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  saturationElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  lightnessElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  hueStepElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  lightnessStepElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  blindnessElement.addEventListener("change", () => {
    applyColorBlindnessFilter(resultElement, blindnessElement.value);
  });
};

const sequentialWithGrayScaleFunction = () => {
  const sequentialWithGrayScaleElement = document.querySelector(
    ".sequential-with-gray-scale"
  );
  if (sequentialWithGrayScaleElement == null) {
    return;
  }
  const hueElement = sequentialWithGrayScaleElement.querySelector<
    HTMLInputElement
  >('[name="hue"]');
  const saturationElement = sequentialWithGrayScaleElement.querySelector<
    HTMLInputElement
  >('[name="saturation"]');
  const lightnessElement = sequentialWithGrayScaleElement.querySelector<
    HTMLInputElement
  >('[name="lightness"]');
  const lightnessStepElement = sequentialWithGrayScaleElement.querySelector<
    HTMLInputElement
  >('[name="lightness-step"]');
  const blindnessElement = sequentialWithGrayScaleElement.querySelector<
    HTMLSelectElement
  >('[name="blindness"]');
  const resultElement = sequentialWithGrayScaleElement.querySelector<
    HTMLOListElement
  >(".result");
  if (
    hueElement == null ||
    saturationElement == null ||
    lightnessElement == null ||
    lightnessStepElement == null ||
    blindnessElement == null ||
    resultElement == null
  ) {
    return;
  }

  const reflectInputValueToList = () => {
    const hue = getNumberValue(hueElement);
    const saturation = getNumberValue(saturationElement);
    const lightness = getNumberValue(lightnessElement);
    const lightnessStep = getNumberValue(lightnessStepElement);
    if (
      hue === "" ||
      saturation === "" ||
      lightness === "" ||
      lightnessStep === ""
    ) {
      return;
    }
    const baseColor: Color = { hue, saturation, lightness };

    const colors = [0, 3, 4, 5, 6, 7].map(n => {
      if (n === 0) {
        return baseColor;
      }
      return {
        ...baseColor,
        saturation: n === 0 ? normalizeSaturation(baseColor.saturation) : 0,
        lightness: normalizeLightness(baseColor.lightness + n * lightnessStep)
      };
    });

    refreshList(colors, resultElement);
  };

  reflectInputValueToList();
  hueElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  saturationElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  lightnessElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  lightnessStepElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  blindnessElement.addEventListener("change", () => {
    applyColorBlindnessFilter(resultElement, blindnessElement.value);
  });
};

const statusHueFunction = () => {
  const statusHueElement = document.querySelector(".status-hue");
  if (statusHueElement == null) {
    return;
  }
  const positiveHueElement = statusHueElement.querySelector<HTMLInputElement>(
    '[name="positive-hue"]'
  );
  const positiveSaturationElement = statusHueElement.querySelector<
    HTMLInputElement
  >('[name="positive-saturation"]');
  const positiveLightnessElement = statusHueElement.querySelector<
    HTMLInputElement
  >('[name="positive-lightness"]');
  const negativeHueElement = statusHueElement.querySelector<HTMLInputElement>(
    '[name="negative-hue"]'
  );
  const negativeSaturationElement = statusHueElement.querySelector<
    HTMLInputElement
  >('[name="negative-saturation"]');
  const negativeLightnessElement = statusHueElement.querySelector<
    HTMLInputElement
  >('[name="negative-lightness"]');
  const blindnessElement = statusHueElement.querySelector<HTMLSelectElement>(
    '[name="blindness"]'
  );
  const resultElement = statusHueElement.querySelector<HTMLOListElement>(
    ".result"
  );
  if (
    positiveHueElement == null ||
    positiveSaturationElement == null ||
    positiveLightnessElement == null ||
    negativeHueElement == null ||
    negativeSaturationElement == null ||
    negativeLightnessElement == null ||
    blindnessElement == null ||
    resultElement == null
  ) {
    return;
  }

  const reflectInputValueToList = () => {
    const positiveHue = getNumberValue(positiveHueElement);
    const positiveSaturation = getNumberValue(positiveSaturationElement);
    const positiveLightness = getNumberValue(positiveLightnessElement);
    const negativeHue = getNumberValue(negativeHueElement);
    const negativeSaturation = getNumberValue(negativeSaturationElement);
    const negativeLightness = getNumberValue(negativeLightnessElement);
    if (
      positiveHue === "" ||
      positiveSaturation === "" ||
      positiveLightness === "" ||
      negativeHue === "" ||
      negativeSaturation === "" ||
      negativeLightness === ""
    ) {
      return;
    }
    const positiveColor: Color = {
      hue: positiveHue,
      saturation: positiveSaturation,
      lightness: positiveLightness
    };
    const negativeColor: Color = {
      hue: negativeHue,
      saturation: negativeSaturation,
      lightness: negativeLightness
    };

    const colors = [undefined, undefined, undefined, undefined, undefined].map(
      (_, index) => {
        return {
          hue: normalizeHue(
            (positiveColor.hue * (4 - index) + negativeColor.hue * index) / 4
          ),
          saturation: normalizeSaturation(
            (positiveColor.saturation * (4 - index) +
              negativeColor.saturation * index) /
              4
          ),
          lightness: normalizeLightness(
            (positiveColor.lightness * (4 - index) +
              negativeColor.lightness * index) /
              4
          )
        };
      }
    );

    refreshList(colors, resultElement);
  };

  reflectInputValueToList();
  positiveHueElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  positiveSaturationElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  positiveLightnessElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  negativeHueElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  negativeSaturationElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  negativeLightnessElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  blindnessElement.addEventListener("change", () => {
    applyColorBlindnessFilter(resultElement, blindnessElement.value);
  });
};

const variableColorFunction = () => {
  const variableColorElement = document.querySelector(".variable-color");
  if (variableColorElement == null) {
    return;
  }
  const hueElement = variableColorElement.querySelector<HTMLInputElement>(
    '[name="hue"]'
  );
  const saturationElement = variableColorElement.querySelector<
    HTMLInputElement
  >('[name="saturation"]');
  const lightnessElement = variableColorElement.querySelector<HTMLInputElement>(
    '[name="lightness"]'
  );
  const hueStepElement = variableColorElement.querySelector<HTMLInputElement>(
    '[name="hue-step"]'
  );
  const saturationStepElement = variableColorElement.querySelector<
    HTMLInputElement
  >('[name="saturation-step"]');
  const lightnessStepElement = variableColorElement.querySelector<
    HTMLInputElement
  >('[name="lightness-step"]');
  const blindnessElement = variableColorElement.querySelector<
    HTMLSelectElement
  >('[name="blindness"]');
  const resultElement = variableColorElement.querySelector<HTMLOListElement>(
    ".result"
  );
  if (
    hueElement == null ||
    saturationElement == null ||
    lightnessElement == null ||
    hueStepElement == null ||
    saturationStepElement == null ||
    lightnessStepElement == null ||
    blindnessElement == null ||
    resultElement == null
  ) {
    return;
  }

  const reflectInputValueToList = () => {
    const hue = getNumberValue(hueElement);
    const saturation = getNumberValue(saturationElement);
    const lightness = getNumberValue(lightnessElement);
    const hueStep = getNumberValue(hueStepElement);
    const saturationStep = getNumberValue(saturationStepElement);
    const lightnessStep = getNumberValue(lightnessStepElement);
    if (
      hue === "" ||
      saturation === "" ||
      lightness === "" ||
      hueStep === "" ||
      saturationStep === "" ||
      lightnessStep === ""
    ) {
      return;
    }
    const baseColor: Color = { hue, saturation, lightness };

    const colors = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(n => {
      return {
        hue: normalizeHue(baseColor.hue + n * hueStep),
        saturation: normalizeSaturation(
          baseColor.saturation + n * saturationStep
        ),
        lightness: normalizeLightness(baseColor.lightness + n * lightnessStep)
      };
    });

    refreshList(colors, resultElement);
  };

  reflectInputValueToList();
  hueElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  saturationElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  lightnessElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  hueStepElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  saturationStepElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  lightnessStepElement.addEventListener("change", () => {
    reflectInputValueToList();
  });
  blindnessElement.addEventListener("change", () => {
    applyColorBlindnessFilter(resultElement, blindnessElement.value);
  });
};

copyColorFunction();
sequentialLightnessFunction();
sequentialHueLightnessFunction();
sequentialWithGrayScaleFunction();
statusHueFunction();
variableColorFunction();
