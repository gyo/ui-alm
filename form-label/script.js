(() => {
  if (location.search === "") {
    return;
  }

  const convertNameToLabel = (formItemName) => {
    if (formItemName === "my-name") {
      return "名前";
    } else if (formItemName === "my-nickname") {
      return "ニックネーム";
    } else if (formItemName === "my-email") {
      return "メールアドレス";
    } else if (formItemName === "my-profile") {
      return "プロフィール";
    } else if (formItemName === "my-un-favorite-animal") {
      return "苦手な動物";
    } else {
      return formItemName;
    }
  };

  const convertValueToLabel = (value) => {
    if (value === "dog") {
      return "犬";
    } else if (value === "cat") {
      return "猫";
    } else if (value === "hamster") {
      return "ハムスター";
    } else if (value === "parrot") {
      return "オウム";
    } else if (value === "spider") {
      return "蜘蛛";
    } else if (value === "goldfish") {
      return "ゴールドフィッシュ";
    } else {
      return decodeURIComponent(value);
    }
  };

  const result = `
    <span>送信した結果</span>
    ${location.search
      .replace("?", "")
      .split("&")
      .map((query) => {
        const [key, value] = query.split("=");
        return `
            <span>
              ${convertNameToLabel(key)}: ${convertValueToLabel(value)}
            </span>
          `;
      })
      .join("")}
  `;

  document.querySelector("output").innerHTML = result;
})();
