"use strict";
const hexRegExp = /^#[0-9a-fA-F]{6}$/;
const isHex = (value) => {
    return hexRegExp.exec(value);
};
const isState = (value) => {
    if (!(typeof value === "object" && value != null)) {
        console.log("value がオブジェクトではありません。");
        return false;
    }
    if (!Object.prototype.hasOwnProperty.call(value, "backgrounds")) {
        console.log("フィールド backgrounds が存在しません。");
        return false;
    }
    if (!Array.isArray(value.backgrounds)) {
        console.log("フィールド backgrounds が配列ではありません。");
        return false;
    }
    if (!value.backgrounds.every((background) => {
        return (typeof background === "string" &&
            (background === "" || isHex(background)));
    })) {
        console.log("backgrounds の要素が期待した形式の文字列ではありません。");
        return false;
    }
    if (!Object.prototype.hasOwnProperty.call(value, "foregrounds")) {
        console.log("フィールド foregrounds が存在しません。");
        return false;
    }
    if (!Array.isArray(value.foregrounds)) {
        console.log("フィールド foregrounds が配列ではありません。");
        return false;
    }
    if (!value.foregrounds.every((foreground) => {
        return (typeof foreground === "string" &&
            (foreground === "" || isHex(foreground)));
    })) {
        console.log("foregrounds の要素が期待した形式の文字列ではありません。");
        return false;
    }
    return true;
};
const createColorField = (type, index, value = "") => {
    const colorFieldElement = document.createElement("div");
    colorFieldElement.classList.add("color-field");
    colorFieldElement.classList.add("js-color-field");
    colorFieldElement.dataset.type = `${type}`;
    colorFieldElement.dataset.index = `${index}`;
    const textInputElement = document.createElement("input");
    textInputElement.value = value;
    textInputElement.classList.add("color-field__text");
    textInputElement.classList.add("js-color-field__text");
    textInputElement.placeholder = "#rrggbb";
    colorFieldElement.appendChild(textInputElement);
    const colorInputElement = document.createElement("input");
    colorInputElement.type = "color";
    colorInputElement.value = value;
    colorInputElement.classList.add("color-field__color");
    colorInputElement.classList.add("js-color-field__color");
    colorFieldElement.appendChild(colorInputElement);
    return colorFieldElement;
};
/**
 * DOM と state を更新
 */
const appendEmptyColor = (type, colors, targetElement) => {
    // DOM を更新
    const colorInputElement = createColorField(type, colors.length);
    targetElement.appendChild(colorInputElement);
    // state を更新
    colors.push("");
};
/**
 * DOM を更新
 */
const renderBackgroundInput = (backgrounds, targetElement) => {
    const fragment = new DocumentFragment();
    let index = 0;
    for (const background of backgrounds) {
        const colorInputElement = createColorField("background", index, background);
        index++;
        fragment.appendChild(colorInputElement);
    }
    // DOM を更新
    targetElement.textContent = "";
    targetElement.appendChild(fragment);
};
/**
 * DOM を更新
 */
const renderForegroundInput = (foregrounds, targetElement) => {
    const fragment = new DocumentFragment();
    let index = 0;
    for (const foreground of foregrounds) {
        const colorInputElement = createColorField("foreground", index, foreground);
        index++;
        fragment.appendChild(colorInputElement);
    }
    // DOM を更新
    targetElement.textContent = "";
    targetElement.appendChild(fragment);
};
/**
 * DOM を更新
 */
const renderResult = (state, targetElement) => {
    const fragmentForResultElement = new DocumentFragment();
    for (const foreground of state.foregrounds) {
        if (!isHex(foreground)) {
            continue;
        }
        const rowElement = document.createElement("div");
        rowElement.classList.add("result__row");
        const fragmentForRowElement = new DocumentFragment();
        for (const background of state.backgrounds) {
            if (!isHex(background)) {
                continue;
            }
            const resultCellDivElement = document.createElement("div");
            resultCellDivElement.classList.add("result__cell");
            resultCellDivElement.style.background = background;
            resultCellDivElement.style.color = foreground;
            resultCellDivElement.textContent = "文字色";
            fragmentForRowElement.appendChild(resultCellDivElement);
        }
        rowElement.appendChild(fragmentForRowElement);
        fragmentForResultElement.appendChild(rowElement);
    }
    // DOM を更新
    targetElement.textContent = "";
    targetElement.appendChild(fragmentForResultElement);
};
const buildInputTextHandler = (state) => (e) => {
    const target = e.target;
    if (!(target instanceof HTMLInputElement) ||
        !target.classList.contains("js-color-field__text")) {
        return;
    }
    const colorFieldElement = target.closest(".js-color-field");
    if (colorFieldElement == null ||
        !(colorFieldElement instanceof HTMLElement)) {
        console.log(".js-color-field が存在しません。");
        return;
    }
    const colorInputElement = colorFieldElement.querySelector(".js-color-field__color");
    if (colorInputElement == null ||
        !(colorInputElement instanceof HTMLInputElement)) {
        console.log(".js-color-field__color が存在しません。");
        return;
    }
    const value = target.value;
    if (!(value === "" || isHex(value))) {
        return;
    }
    const type = colorFieldElement.dataset.type;
    const index = parseInt(colorFieldElement.dataset.index ?? "", 10);
    const stateKey = type === "background" ? "backgrounds" : "foregrounds";
    const colors = state[stateKey];
    // state を更新
    // 入力値を state へ反映する
    colors[index] = value;
    // DOM を更新
    // 入力値を他の DOM へ反映する
    colorInputElement.value = value !== "" ? value : "#000000";
    if (value === "" || index !== colors.length - 1) {
        return;
    }
    const inputAreaElement = target.closest(".js-input-area");
    if (inputAreaElement == null || !(inputAreaElement instanceof HTMLElement)) {
        console.log(".js-input-area が存在しません。");
        return;
    }
    // DOM と state を更新
    appendEmptyColor(type, colors, inputAreaElement);
};
const buildChangeColorHandler = (state) => (e) => {
    const target = e.target;
    if (!(target instanceof HTMLInputElement) ||
        !target.classList.contains("js-color-field__color")) {
        return;
    }
    const value = target.value;
    const colorFieldElement = target.closest(".js-color-field");
    if (colorFieldElement == null ||
        !(colorFieldElement instanceof HTMLElement)) {
        console.log(".js-color-field が存在しません。");
        return;
    }
    const textInputElement = colorFieldElement.querySelector(".js-color-field__text");
    if (textInputElement == null ||
        !(textInputElement instanceof HTMLInputElement)) {
        console.log(".js-color-field__text が存在しません。");
        return;
    }
    // DOM を更新
    // 入力値を他の DOM へ反映する
    textInputElement.value = value;
    const type = colorFieldElement.dataset.type;
    const index = parseInt(colorFieldElement.dataset.index ?? "", 10);
    const stateKey = type === "background" ? "backgrounds" : "foregrounds";
    const colors = state[stateKey];
    // state を更新
    // 入力値を state へ反映する
    colors[index] = value;
    if (value === "" || index !== colors.length - 1) {
        return;
    }
    const inputAreaElement = target.closest(".js-input-area");
    if (inputAreaElement == null || !(inputAreaElement instanceof HTMLElement)) {
        console.log(".js-input-area が存在しません。");
        return;
    }
    // DOM と state を更新
    appendEmptyColor(type, colors, inputAreaElement);
};
const buildSubmitHandler = (state, resultElement, importExportInputElement) => (e) => {
    e.preventDefault();
    // DOM を更新
    // 視覚表現のための DOM を更新する
    renderResult(state, resultElement);
    // DOM を更新
    // インポートエクスポートのための DOM を更新する
    importExportInputElement.value = JSON.stringify(state);
};
const buildImportHandler = (state, backgroundInputAreaElement, foregroundInputAreaElement, resultElement) => (e) => {
    const target = e.target;
    if (target == null || !(target instanceof HTMLElement)) {
        console.log("e.target が存在しません。");
        return;
    }
    const parentElement = target.parentElement;
    if (parentElement == null || !(parentElement instanceof HTMLElement)) {
        console.log("e.target の parentElement が存在しません。");
        return;
    }
    const textAreaElement = parentElement.querySelector(".js-import-export-input");
    if (textAreaElement == null ||
        !(textAreaElement instanceof HTMLTextAreaElement)) {
        console.log(".js-import-export-input が存在しません。");
        return;
    }
    const importValue = textAreaElement.value;
    const importObject = JSON.parse(importValue);
    if (!isState(importObject)) {
        console.log("インポート可能な形式ではありません。");
        return;
    }
    // state を更新
    state.backgrounds = importObject.backgrounds;
    state.foregrounds = importObject.foregrounds;
    // DOM を更新
    renderBackgroundInput(state.backgrounds, backgroundInputAreaElement);
    renderForegroundInput(state.foregrounds, foregroundInputAreaElement);
    renderResult(state, resultElement);
};
(() => {
    const formElement = document.querySelector(".js-form");
    if (formElement == null) {
        throw new Error("formElement is null");
    }
    const backgroundInputAreaElement = document.querySelector(".js-background-input-area");
    if (backgroundInputAreaElement == null) {
        throw new Error("backgroundInputAreaElement is null");
    }
    const foregroundInputAreaElement = document.querySelector(".js-foreground-input-area");
    if (foregroundInputAreaElement == null) {
        throw new Error("foregroundInputAreaElement is null");
    }
    const resultElement = document.querySelector(".js-result");
    if (resultElement == null) {
        throw new Error("resultElement is null");
    }
    const importExportInputElement = document.querySelector(".js-import-export-input");
    if (importExportInputElement == null ||
        !(importExportInputElement instanceof HTMLTextAreaElement)) {
        throw new Error("importExportInputElement is null");
    }
    const importElement = document.querySelector(".js-import");
    if (importElement == null) {
        throw new Error("importElement is null");
    }
    const state = {
        backgrounds: [""],
        foregrounds: [""],
    };
    renderBackgroundInput(state.backgrounds, backgroundInputAreaElement);
    renderForegroundInput(state.foregrounds, foregroundInputAreaElement);
    renderResult(state, resultElement);
    importExportInputElement.value = JSON.stringify(state);
    formElement.addEventListener("submit", buildSubmitHandler(state, resultElement, importExportInputElement));
    importElement.addEventListener("click", buildImportHandler(state, backgroundInputAreaElement, foregroundInputAreaElement, resultElement));
    document.addEventListener("input", buildInputTextHandler(state));
    document.addEventListener("change", buildChangeColorHandler(state));
})();
