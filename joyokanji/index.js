const form = document.querySelector("form");
const textarea = form.querySelector("textarea");
const countElement = document.querySelector('output[name="count"]');
const outsideElement = document.querySelector('output[name="outside"]');
const insideElement = document.querySelector('output[name="inside"]');

const zeroOutside = "表外漢字はありません。";
const zeroInside = "表内漢字はありません。";

const createText = (type, kanjis) => {
  const count = kanjis.length;
  const kind = new Set(kanjis).size;

  return (
    type +
    "漢字が" +
    kind +
    "種類" +
    count +
    "個あります。「" +
    kanjis.join(", ") +
    "」"
  );
};

const check = () => {
  const value = textarea.value;

  // 入力された文章中の漢字を取得する
  const characters = value.match(/[\u4E00-\u9FFF]/g);

  if (characters == null) {
    countElement.textContent = value.length + "文字中に0文字の漢字があります。";
    outsideElement.textContent = zeroOutside;
    insideElement.textContent = zeroInside;
    return;
  }

  // 表内漢字と表外漢字に分類する
  const outsideCharacters = [];
  const insideCharacters = [];
  for (const character of characters) {
    if (joyokanji[character] === 1) {
      insideCharacters.push(character);
    } else {
      outsideCharacters.push(character);
    }
  }

  let outputOutsideText = "";
  if (outsideCharacters.length === 0) {
    outputOutsideText = zeroOutside;
  } else {
    outputOutsideText = createText("表外", outsideCharacters);
  }

  let outputInsideText = "";
  if (insideCharacters.length === 0) {
    outputInsideText = zeroInside;
  } else {
    outputInsideText = createText("表内", insideCharacters);
  }

  // DOM に反映する
  countElement.textContent =
    value.length + "文字中に" + characters.length + "文字の漢字があります。";
  outsideElement.textContent = outputOutsideText;
  insideElement.textContent = outputInsideText;
};

const debounce = (callback) => {
  let timer;

  return () => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      callback();
    }, 200);
  };
};

check();
textarea.addEventListener(
  "input",
  debounce(() => check())
);
