const fieldsetElements = Array.from(document.querySelectorAll(".js-fieldset"));
const sectionElements = Array.from(document.querySelectorAll(".js-section"));
const stackedInputElement = document.querySelector(`input[name="stacked"]`);
const visibleInputElements = Array.from(
  document.querySelectorAll(`.js-fieldset input[name="visible"]`)
);
const opacityInputElements = Array.from(
  document.querySelectorAll(`.js-fieldset input[name="opacity"]`)
);

const fieldsetElementsToState = (fieldsetElements) => {
  const result = {};
  result.stacked = stackedInputElement.checked;
  result.sections = {};
  for (const fieldsetElement of fieldsetElements) {
    result.sections[fieldsetElement.dataset.fontFamily] = {
      visible: fieldsetElement.querySelector(`input[name="visible"]`).checked,
      opacity: fieldsetElement.querySelector(`input[name="opacity"]`).value,
    };
  }
  return result;
};

const stateToSectionElements = (state, sectionElements) => {
  for (const sectionElement of sectionElements) {
    if (state.stacked) {
      sectionElement.style.position = "absolute";
    } else {
      sectionElement.style.position = "static";
    }
    const { visible, opacity } = state.sections[
      sectionElement.dataset.fontFamily
    ];
    if (visible) {
      sectionElement.style.display = "block";
    } else {
      sectionElement.style.display = "none";
    }
    sectionElement.style.opacity = opacity / 100;
  }
};

const render = () => {
  const state = fieldsetElementsToState(fieldsetElements);
  stateToSectionElements(
    state,
    sectionElements
  );
};

render();

stackedInputElement.addEventListener("input", () => {
  render();
});

for (const visibleInputElement of visibleInputElements) {
  visibleInputElement.addEventListener("input", () => {
    render();
  });
}

for (const opacityInputElement of opacityInputElements) {
  opacityInputElement.addEventListener("input", () => {
    render();
  });
}
