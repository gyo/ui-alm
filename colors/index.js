"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var colorBlindnessTypes = [
    "protanopia",
    "protanomaly",
    "deuteranopia",
    "deuteranomaly",
    "tritanopia",
    "tritanomaly",
    "achromatopsia",
    "achromatomaly"
];
var normalizeHue = function (hue) {
    var h = hue;
    while (h < 0) {
        h = h + 360;
    }
    return Math.round(h % 360);
};
var normalizeSaturation = function (saturation) {
    if (saturation < 0) {
        return 0;
    }
    if (100 < saturation) {
        return 100;
    }
    return Math.round(saturation);
};
var normalizeLightness = function (lightness) {
    if (lightness < 0) {
        return 0;
    }
    if (100 < lightness) {
        return 100;
    }
    return Math.round(lightness);
};
var getHslStyleValue = function (color) {
    return "hsl(" + color.hue + ", " + color.saturation + "%, " + color.lightness + "%)";
};
var getHexStyleValue = function (color) {
    return HSLToHex(color.hue, color.saturation, color.lightness);
};
var getNumberValue = function (element) {
    var value = element.value;
    if (value === "") {
        return "";
    }
    var number = parseInt(value);
    if (isNaN(number)) {
        return "";
    }
    return number;
};
var applyColorBlindnessFilter = function (element, type) {
    var _a;
    (_a = element.classList).remove.apply(_a, colorBlindnessTypes);
    if (type !== "") {
        element.classList.add(type);
    }
};
var removeChildren = function (element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
};
var refreshList = function (colors, element) {
    removeChildren(element);
    var fragment = document.createDocumentFragment();
    colors.forEach(function (color, index) {
        var hslStyleValue = getHslStyleValue(color);
        var hexStyleValue = getHexStyleValue(color);
        var li = document.createElement("li");
        var tile = document.createElement("div");
        tile.classList.add("tile");
        tile.textContent = "" + index;
        tile.style.backgroundColor = hslStyleValue;
        li.appendChild(tile);
        var hsl = document.createElement("div");
        hsl.classList.add("hsl");
        hsl.textContent = hslStyleValue;
        li.appendChild(hsl);
        var hex = document.createElement("div");
        hex.classList.add("hex");
        hex.textContent = hexStyleValue;
        li.appendChild(hex);
        fragment.appendChild(li);
    });
    element.appendChild(fragment);
};
// https://css-tricks.com/converting-color-spaces-in-javascript/
var HSLToHex = function (h, s, l) {
    s /= 100;
    l /= 100;
    var c = (1 - Math.abs(2 * l - 1)) * s, x = c * (1 - Math.abs(((h / 60) % 2) - 1)), m = l - c / 2, r = 0, g = 0, b = 0;
    if (0 <= h && h < 60) {
        r = c;
        g = x;
        b = 0;
    }
    else if (60 <= h && h < 120) {
        r = x;
        g = c;
        b = 0;
    }
    else if (120 <= h && h < 180) {
        r = 0;
        g = c;
        b = x;
    }
    else if (180 <= h && h < 240) {
        r = 0;
        g = x;
        b = c;
    }
    else if (240 <= h && h < 300) {
        r = x;
        g = 0;
        b = c;
    }
    else if (300 <= h && h < 360) {
        r = c;
        g = 0;
        b = x;
    }
    // Having obtained RGB, convert channels to hex
    var sr = Math.round((r + m) * 255).toString(16);
    var sg = Math.round((g + m) * 255).toString(16);
    var sb = Math.round((b + m) * 255).toString(16);
    // Prepend 0s, if necessary
    if (sr.length == 1)
        sr = "0" + sr;
    if (sg.length == 1)
        sg = "0" + sg;
    if (sb.length == 1)
        sb = "0" + sb;
    return "#" + sr + sg + sb;
};
var copyColorFunction = function () {
    var timerId;
    var notification = document.querySelector(".notification");
    var listElements = document.querySelectorAll("ol");
    if (notification == null) {
        return;
    }
    listElements.forEach(function (listElement) {
        listElement.addEventListener("click", function (e) {
            var target = e.target;
            if (target == null) {
                return;
            }
            var classList = target.classList;
            if (target.tagName === "DIV" &&
                (classList.contains("hsl") || classList.contains("hex")) &&
                target.textContent != null) {
                var trimmedTextContent_1 = target.textContent.trim();
                navigator.clipboard.writeText(trimmedTextContent_1).then(function () {
                    if (timerId != null) {
                        clearTimeout(timerId);
                    }
                    notification.classList.add("notification_open");
                    notification.textContent = trimmedTextContent_1 + " \u3092 Clipboard \u306B\u30B3\u30D4\u30FC\u3057\u307E\u3057\u305F\u3002";
                    timerId = setTimeout(function () {
                        notification.classList.remove("notification_open");
                        notification.textContent = "";
                    }, 1250);
                }, function () {
                    if (timerId != null) {
                        clearTimeout(timerId);
                    }
                    notification.classList.add("notification_error");
                    notification.classList.add("notification_open");
                    notification.textContent = "Clipboard へのコピーに失敗しました。";
                    timerId = setTimeout(function () {
                        notification.classList.remove("notification_open");
                        notification.textContent = "";
                    }, 1250);
                });
            }
        });
    });
};
var sequentialLightnessFunction = function () {
    var sequentialLightnessElement = document.querySelector(".sequential-lightness");
    if (sequentialLightnessElement == null) {
        return;
    }
    var hueElement = sequentialLightnessElement.querySelector('[name="hue"]');
    var saturationElement = sequentialLightnessElement.querySelector('[name="saturation"]');
    var lightnessElement = sequentialLightnessElement.querySelector('[name="lightness"]');
    var lightnessStepElement = sequentialLightnessElement.querySelector('[name="lightness-step"]');
    var blindnessElement = sequentialLightnessElement.querySelector('[name="blindness"]');
    var resultElement = sequentialLightnessElement.querySelector(".result");
    if (hueElement == null ||
        saturationElement == null ||
        lightnessElement == null ||
        lightnessStepElement == null ||
        blindnessElement == null ||
        resultElement == null) {
        return;
    }
    var reflectInputValueToList = function () {
        var hue = getNumberValue(hueElement);
        var saturation = getNumberValue(saturationElement);
        var lightness = getNumberValue(lightnessElement);
        var lightnessStep = getNumberValue(lightnessStepElement);
        if (hue === "" ||
            saturation === "" ||
            lightness === "" ||
            lightnessStep === "") {
            return;
        }
        var baseColor = { hue: hue, saturation: saturation, lightness: lightness };
        var colors = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(function (n) {
            return __assign(__assign({}, baseColor), { lightness: normalizeLightness(baseColor.lightness + n * lightnessStep) });
        });
        refreshList(colors, resultElement);
    };
    reflectInputValueToList();
    hueElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    saturationElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    lightnessElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    lightnessStepElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    blindnessElement.addEventListener("change", function () {
        applyColorBlindnessFilter(resultElement, blindnessElement.value);
    });
};
var sequentialHueLightnessFunction = function () {
    var sequentialHueLightnessElement = document.querySelector(".sequential-hue-lightness");
    if (sequentialHueLightnessElement == null) {
        return;
    }
    var hueElement = sequentialHueLightnessElement.querySelector('[name="hue"]');
    var saturationElement = sequentialHueLightnessElement.querySelector('[name="saturation"]');
    var lightnessElement = sequentialHueLightnessElement.querySelector('[name="lightness"]');
    var hueStepElement = sequentialHueLightnessElement.querySelector('[name="hue-step"]');
    var lightnessStepElement = sequentialHueLightnessElement.querySelector('[name="lightness-step"]');
    var blindnessElement = sequentialHueLightnessElement.querySelector('[name="blindness"]');
    var resultElement = sequentialHueLightnessElement.querySelector(".result");
    if (hueElement == null ||
        saturationElement == null ||
        lightnessElement == null ||
        hueStepElement == null ||
        lightnessStepElement == null ||
        blindnessElement == null ||
        resultElement == null) {
        return;
    }
    var reflectInputValueToList = function () {
        var hue = getNumberValue(hueElement);
        var saturation = getNumberValue(saturationElement);
        var lightness = getNumberValue(lightnessElement);
        var hueStep = getNumberValue(hueStepElement);
        var lightnessStep = getNumberValue(lightnessStepElement);
        if (hue === "" ||
            saturation === "" ||
            lightness === "" ||
            hueStep === "" ||
            lightnessStep === "") {
            return;
        }
        var baseColor = { hue: hue, saturation: saturation, lightness: lightness };
        var colors = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(function (n) {
            return __assign(__assign({}, baseColor), { hue: normalizeHue(baseColor.hue + n * hueStep), lightness: normalizeLightness(baseColor.lightness + n * lightnessStep) });
        });
        refreshList(colors, resultElement);
    };
    reflectInputValueToList();
    hueElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    saturationElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    lightnessElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    hueStepElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    lightnessStepElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    blindnessElement.addEventListener("change", function () {
        applyColorBlindnessFilter(resultElement, blindnessElement.value);
    });
};
var sequentialWithGrayScaleFunction = function () {
    var sequentialWithGrayScaleElement = document.querySelector(".sequential-with-gray-scale");
    if (sequentialWithGrayScaleElement == null) {
        return;
    }
    var hueElement = sequentialWithGrayScaleElement.querySelector('[name="hue"]');
    var saturationElement = sequentialWithGrayScaleElement.querySelector('[name="saturation"]');
    var lightnessElement = sequentialWithGrayScaleElement.querySelector('[name="lightness"]');
    var lightnessStepElement = sequentialWithGrayScaleElement.querySelector('[name="lightness-step"]');
    var blindnessElement = sequentialWithGrayScaleElement.querySelector('[name="blindness"]');
    var resultElement = sequentialWithGrayScaleElement.querySelector(".result");
    if (hueElement == null ||
        saturationElement == null ||
        lightnessElement == null ||
        lightnessStepElement == null ||
        blindnessElement == null ||
        resultElement == null) {
        return;
    }
    var reflectInputValueToList = function () {
        var hue = getNumberValue(hueElement);
        var saturation = getNumberValue(saturationElement);
        var lightness = getNumberValue(lightnessElement);
        var lightnessStep = getNumberValue(lightnessStepElement);
        if (hue === "" ||
            saturation === "" ||
            lightness === "" ||
            lightnessStep === "") {
            return;
        }
        var baseColor = { hue: hue, saturation: saturation, lightness: lightness };
        var colors = [0, 3, 4, 5, 6, 7].map(function (n) {
            if (n === 0) {
                return baseColor;
            }
            return __assign(__assign({}, baseColor), { saturation: n === 0 ? normalizeSaturation(baseColor.saturation) : 0, lightness: normalizeLightness(baseColor.lightness + n * lightnessStep) });
        });
        refreshList(colors, resultElement);
    };
    reflectInputValueToList();
    hueElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    saturationElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    lightnessElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    lightnessStepElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    blindnessElement.addEventListener("change", function () {
        applyColorBlindnessFilter(resultElement, blindnessElement.value);
    });
};
var statusHueFunction = function () {
    var statusHueElement = document.querySelector(".status-hue");
    if (statusHueElement == null) {
        return;
    }
    var positiveHueElement = statusHueElement.querySelector('[name="positive-hue"]');
    var positiveSaturationElement = statusHueElement.querySelector('[name="positive-saturation"]');
    var positiveLightnessElement = statusHueElement.querySelector('[name="positive-lightness"]');
    var negativeHueElement = statusHueElement.querySelector('[name="negative-hue"]');
    var negativeSaturationElement = statusHueElement.querySelector('[name="negative-saturation"]');
    var negativeLightnessElement = statusHueElement.querySelector('[name="negative-lightness"]');
    var blindnessElement = statusHueElement.querySelector('[name="blindness"]');
    var resultElement = statusHueElement.querySelector(".result");
    if (positiveHueElement == null ||
        positiveSaturationElement == null ||
        positiveLightnessElement == null ||
        negativeHueElement == null ||
        negativeSaturationElement == null ||
        negativeLightnessElement == null ||
        blindnessElement == null ||
        resultElement == null) {
        return;
    }
    var reflectInputValueToList = function () {
        var positiveHue = getNumberValue(positiveHueElement);
        var positiveSaturation = getNumberValue(positiveSaturationElement);
        var positiveLightness = getNumberValue(positiveLightnessElement);
        var negativeHue = getNumberValue(negativeHueElement);
        var negativeSaturation = getNumberValue(negativeSaturationElement);
        var negativeLightness = getNumberValue(negativeLightnessElement);
        if (positiveHue === "" ||
            positiveSaturation === "" ||
            positiveLightness === "" ||
            negativeHue === "" ||
            negativeSaturation === "" ||
            negativeLightness === "") {
            return;
        }
        var positiveColor = {
            hue: positiveHue,
            saturation: positiveSaturation,
            lightness: positiveLightness
        };
        var negativeColor = {
            hue: negativeHue,
            saturation: negativeSaturation,
            lightness: negativeLightness
        };
        var colors = [undefined, undefined, undefined, undefined, undefined].map(function (_, index) {
            return {
                hue: normalizeHue((positiveColor.hue * (4 - index) + negativeColor.hue * index) / 4),
                saturation: normalizeSaturation((positiveColor.saturation * (4 - index) +
                    negativeColor.saturation * index) /
                    4),
                lightness: normalizeLightness((positiveColor.lightness * (4 - index) +
                    negativeColor.lightness * index) /
                    4)
            };
        });
        refreshList(colors, resultElement);
    };
    reflectInputValueToList();
    positiveHueElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    positiveSaturationElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    positiveLightnessElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    negativeHueElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    negativeSaturationElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    negativeLightnessElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    blindnessElement.addEventListener("change", function () {
        applyColorBlindnessFilter(resultElement, blindnessElement.value);
    });
};
var variableColorFunction = function () {
    var variableColorElement = document.querySelector(".variable-color");
    if (variableColorElement == null) {
        return;
    }
    var hueElement = variableColorElement.querySelector('[name="hue"]');
    var saturationElement = variableColorElement.querySelector('[name="saturation"]');
    var lightnessElement = variableColorElement.querySelector('[name="lightness"]');
    var hueStepElement = variableColorElement.querySelector('[name="hue-step"]');
    var saturationStepElement = variableColorElement.querySelector('[name="saturation-step"]');
    var lightnessStepElement = variableColorElement.querySelector('[name="lightness-step"]');
    var blindnessElement = variableColorElement.querySelector('[name="blindness"]');
    var resultElement = variableColorElement.querySelector(".result");
    if (hueElement == null ||
        saturationElement == null ||
        lightnessElement == null ||
        hueStepElement == null ||
        saturationStepElement == null ||
        lightnessStepElement == null ||
        blindnessElement == null ||
        resultElement == null) {
        return;
    }
    var reflectInputValueToList = function () {
        var hue = getNumberValue(hueElement);
        var saturation = getNumberValue(saturationElement);
        var lightness = getNumberValue(lightnessElement);
        var hueStep = getNumberValue(hueStepElement);
        var saturationStep = getNumberValue(saturationStepElement);
        var lightnessStep = getNumberValue(lightnessStepElement);
        if (hue === "" ||
            saturation === "" ||
            lightness === "" ||
            hueStep === "" ||
            saturationStep === "" ||
            lightnessStep === "") {
            return;
        }
        var baseColor = { hue: hue, saturation: saturation, lightness: lightness };
        var colors = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(function (n) {
            return {
                hue: normalizeHue(baseColor.hue + n * hueStep),
                saturation: normalizeSaturation(baseColor.saturation + n * saturationStep),
                lightness: normalizeLightness(baseColor.lightness + n * lightnessStep)
            };
        });
        refreshList(colors, resultElement);
    };
    reflectInputValueToList();
    hueElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    saturationElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    lightnessElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    hueStepElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    saturationStepElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    lightnessStepElement.addEventListener("change", function () {
        reflectInputValueToList();
    });
    blindnessElement.addEventListener("change", function () {
        applyColorBlindnessFilter(resultElement, blindnessElement.value);
    });
};
copyColorFunction();
sequentialLightnessFunction();
sequentialHueLightnessFunction();
sequentialWithGrayScaleFunction();
statusHueFunction();
variableColorFunction();
