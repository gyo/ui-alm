const flexibleTextareaElements = Array.from(
  document.querySelectorAll(".flexible-textarea")
);

const getProperties = (flexibleTextareaElement) => {
  const min = flexibleTextareaElement.dataset.min ?? "1";
  const max = flexibleTextareaElement.dataset.max;
  const flexibleTextareaDummyElement = flexibleTextareaElement.querySelector(
    ".flexible-textarea__dummy"
  );
  const flexibleTextareaTextareaElement = flexibleTextareaElement.querySelector(
    ".flexible-textarea__textarea"
  );

  return {
    min,
    max,
    flexibleTextareaDummyElement,
    flexibleTextareaTextareaElement,
  };
};

for (const flexibleTextareaElement of flexibleTextareaElements) {
  const {
    min,
    max,
    flexibleTextareaDummyElement,
    flexibleTextareaTextareaElement,
  } = getProperties(flexibleTextareaElement);

  flexibleTextareaDummyElement.style.minHeight = `calc(var(--paddingTopBottom) * 2 + var(--font-size) * var(--line-height) * ${min})`;
  if (max != null) {
    flexibleTextareaDummyElement.style.maxHeight = `calc(var(--paddingTopBottom) * 2 + var(--font-size) * var(--line-height) * ${max})`;
  }
  flexibleTextareaDummyElement.textContent =
    flexibleTextareaTextareaElement.value + "\u200b";

  flexibleTextareaTextareaElement.addEventListener("input", (e) => {
    flexibleTextareaDummyElement.textContent = e.target.value + "\u200b";
  });
}
