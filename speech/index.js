const input = document.querySelector("input");
const select = document.querySelector("select");
const textarea = document.querySelector("textarea");
const button = document.querySelector("button");

const voices = [];

const appendVoices = () => {
  const vs = speechSynthesis.getVoices();

  for (const v of vs) {
    voices.push(v);

    const option = document.createElement("option");
    option.textContent = v.name + " (" + v.lang + ")";
    option.setAttribute("data-lang", v.lang);
    option.setAttribute("data-name", v.name);
    select.appendChild(option);
  }
};

speechSynthesis.onvoiceschanged = appendVoices;

button.addEventListener("click", (e) => {
  e.preventDefault();

  const rate = parseInt(input.value, 10) / 100;
  const selectedOption = select.selectedOptions[0];

  const speechSynthesisUtterance = new SpeechSynthesisUtterance(textarea.value);
  speechSynthesisUtterance.rate = rate;
  speechSynthesisUtterance.lang = selectedOption.dataset.lang;
  speechSynthesisUtterance.voice = voices.find((voice) => {
    return voice.name === selectedOption.dataset.name;
  });

  speechSynthesis.speak(speechSynthesisUtterance);
});
