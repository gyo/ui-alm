{
  const parser = new UAParser();
  const result = parser.getResult();
  if (result.os.name === "iOS") {
    const metaViewportElement = document.querySelector(`meta[name="viewport"]`);
    if (metaViewportElement != null) {
      const metaViewportElementContent = metaViewportElement.getAttribute(
        "content"
      );
      metaViewportElement.setAttribute(
        "content",
        metaViewportElementContent + ", user-scalable=no"
      );
    }
  }
}
