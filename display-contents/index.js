const wrappers = Array.from(document.querySelectorAll(".wrapper"));
const buttons = Array.from(document.querySelectorAll("button"));

for (const wrapper of wrappers) {
  console.log(wrapper);

  const inner = wrapper.querySelector(".inner");

  console.log(inner);

  wrapper.addEventListener("click", (e) => {
    if (e.target === wrapper) {
      console.log(wrapper);
    }
  });

  inner.addEventListener("click", (e) => {
    if (e.target === inner) {
      console.log(inner);
    }
  });
}

for (const button of buttons) {
  button.addEventListener("click", (e) => {
    console.log(e.target);
  });
}
